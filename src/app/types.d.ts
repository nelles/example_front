export type campaign = {
   id: number;
   name: string;
   description: string;
   goal: number;
   status: string;
};
