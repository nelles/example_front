import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import campaignReducer from "../features/campaign/campaignSlice";
import snackbarReducer from "../features/snackbar/snackbarSlice";
import donationReducer from "../features/donation/donationSlice";
import sessionReducer from "../features/session/sessionSlice";
import dialogReducer from "../features/dialog/dialogSlice";

export const store = configureStore({
   reducer: {
      snackbar: snackbarReducer,
      campaign: campaignReducer,
      donation: donationReducer,
      session: sessionReducer,
      dialog: dialogReducer,
   },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
   ReturnType,
   RootState,
   unknown,
   Action<string>
>;
