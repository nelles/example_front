import * as React from "react";
import Dialog from "@mui/material/Dialog";
import Slide from "@mui/material/Slide";
import { TransitionProps } from "@mui/material/transitions";
import { DialogState } from "../../features/dialog/dialog";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { clearDialog } from "../../features/dialog/dialogSlice";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import Grid from "@mui/material/Grid";
import { Alert, AlertColor, Button, TextField } from "@mui/material";
import Snackbar from "@mui/material/Snackbar";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import { useEffect, useState } from "react";
import { apiPost } from "../../utilities/ApiRequest";
import { setCampaign } from "../../features/campaign/campaignSlice";
import { setDonation } from "../../features/donation/donationSlice";
import { formatter } from "../../utilities/Functions";
//import { setSnackbar } from "../../features/snackbar/snackbarSlice";

const Transition = React.forwardRef(function Transition(
   props: TransitionProps & {
      children: React.ReactElement<any, any>;
   },
   ref: React.Ref<unknown>
) {
   return <Slide direction='up' ref={ref} {...props} />;
});

export default function DiaWrap(): JSX.Element {
   const dis = useAppDispatch();
   const dialog: DialogState = useAppSelector((state) => state.dialog);
   const campaign: any = useAppSelector((state) => state.campaign);
   const donation: any = useAppSelector((state) => state.donation);
   const { open, content, params, title } = dialog;

   const [nickname, setNickname] = useState("");
   const [amount, setAmount] = useState("");
   const [opens, setOpens] = useState(false);
   const [message, setMessage] = useState("");
   const [loading, setLoading] = useState(false);
   const [success, setSuccess] = useState(false);
   const [severity, setSeverity] = useState<AlertColor>("success");
   const timer = React.useRef<number>();

   const isValidNickName = (nickname: string) => {
      const re = /^[a-zA-Z][a-zA-Z0-9_]*$/;
      if (!re.test(nickname)) {
         setMessage(
            "Nickname may contain English letters, digits, and underscores."
         );
         return false;
      } else {
         return true;
      }
   };

   const isValidNumber = (amount: string) => {
      // make sure it is a number
      const re = /^[0-9]*$/;
      if (!re.test(amount)) {
         setMessage("Amount must be a number");
         return false;
      } else {
         return true;
      }
   };

   const handleButtonClick = async () => {
      if (!loading) {
         setSuccess(false);
         setLoading(true);
         if (!isValidNumber(amount) || !isValidNickName(nickname)) {
            setSeverity("error");
            setOpens(true);
            setSuccess(true);
            setLoading(false);
         } else {
            setSeverity("info");
            setMessage("Processing...");
            const donationRes: any = await apiPost("/donate_add", {
               nickname,
               amount,
               cid: params.id,
            });
            let newDonation = donation;
            const newArr = [...donation.arr, donationRes.data.donation];

            dis(setDonation({ ...donation, arr: newArr }));
            timer.current = window.setTimeout(() => {
               setSeverity("success");
               setMessage("Thank you for your donation");
               setNickname("");
               setAmount("");
               setOpens(true);
               setSuccess(true);
               setLoading(false);
            }, 1000);
            timer.current = window.setTimeout(() => handleClose(), 2000);
         }
      }
   };

   const handleCloses = (
      event: React.SyntheticEvent | Event,
      reason?: string
   ) => {
      if (reason === "clickaway") {
         return;
      }

      setOpens(false);
   };

   const handleClose = () => {
      dis(clearDialog());
   };

   const action = (
      <React.Fragment>
         <IconButton
            size='small'
            aria-label='close'
            color='inherit'
            onClick={handleCloses}
         >
            <CloseIcon fontSize='small' />
         </IconButton>
      </React.Fragment>
   );

   const existing = "";

   useEffect(() => {
      //refresh
   }, [nickname, amount]);

   return (
      <>
         <Dialog
            open={open}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleClose}
         >
            <div style={{ padding: 20, minHeight: 350, minWidth: 200 }}>
               <div style={{ width: "100%" }}>{title}</div>
               <div
                  style={{
                     width: "100%",
                     borderRadius: 5,
                     backgroundColor: "#eee",
                     padding: 15,
                     margin: 2,
                  }}
               >
                  <Grid container spacing={1}>
                     <Grid item xs={12}>
                        Make a donation
                     </Grid>

                     <Grid item xs={12}>
                        <TextField
                           id='outlined-basic'
                           label='nickname'
                           variant='outlined'
                           size='small'
                           fullWidth
                           onChange={(e) => setNickname(e.target.value)}
                        />
                     </Grid>

                     <Grid item xs={12}>
                        <TextField
                           size='small'
                           id='outlined-basic'
                           label='amount'
                           variant='outlined'
                           fullWidth
                           onChange={(e) => setAmount(e.target.value)}
                        />
                     </Grid>

                     <Grid item xs={12}></Grid>
                  </Grid>
                  <Box sx={{ position: "relative" }}>
                     <Button
                        variant='contained'
                        disabled={loading}
                        fullWidth
                        onClick={handleButtonClick}
                     >
                        Add Donation
                     </Button>
                     {loading && (
                        <CircularProgress
                           size={24}
                           sx={{
                              position: "absolute",
                              top: "50%",
                              left: "50%",
                              marginTop: "-12px",
                              marginLeft: "-12px",
                           }}
                        />
                     )}
                  </Box>
               </div>
               <div style={{ width: "100%", height: 300, overflowY: "scroll" }}>
                  Existing Donations
                  <table style={{ width: "100%" }}>
                     <tr>
                        <td>
                           <b>NickName</b>
                        </td>
                        <td>
                           <b>Amount</b>
                        </td>
                     </tr>

                     {donation.arr.map((item: any) => {
                        if (item.cid === params.id) {
                           return (
                              <tr>
                                 <td>{item.nickname}</td>
                                 <td>{formatter.format(item.amount)}</td>
                              </tr>
                           );
                        }
                     })}
                  </table>
               </div>
            </div>
         </Dialog>
         <Snackbar
            open={opens}
            autoHideDuration={5000}
            onClose={handleCloses}
            action={action}
         >
            <Alert sx={{ width: "100%" }} severity={severity}>
               {message}
            </Alert>
         </Snackbar>
      </>
   );
}
