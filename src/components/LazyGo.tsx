import React from "react";
import LazyLoad from "react-lazyload";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { Button, Grid } from "@mui/material";
import { setDialog } from "../features/dialog/dialogSlice";
import FadeInSection from "./FadeInSection";
import { formatter } from "../utilities/Functions";

interface RowProps {
   id: number;
   name: string;
   description: string;
   goal: number;
   status: string;
   openDia: any;
   donation: any;
}

const DonRow = (props: RowProps) => {
   // this component needs to fade out the further from the center of the window it is

   const { id, name, description, goal, openDia, donation } = props;
   let total: number = 0;
   donation.arr.map((don: any) => {
      if (don.cid === id) total += parseInt(don.amount);
   });

   const status = total >= goal ? "successful" : "active";
   return (
      <div key={id} style={{ marginBottom: 20, backgroundColor: "#ddd" }}>
         <Grid container spacing={1}>
            <Grid item xs={7} sx={{ m: 1 }}>
               {name} <i>(status: {status})</i>
            </Grid>
            <Grid item xs={5} sx={{ m: 1 }}>
               GOAL: {formatter.format(goal)} | TOTAL: {formatter.format(total)}
            </Grid>
            <Grid item xs={12} sx={{ m: 1 }}>
               Description: {description}
            </Grid>
            <Grid item xs={12} sx={{ m: 2 }}>
               <Button
                  fullWidth
                  variant='contained'
                  onClick={openDia(id, name, goal)}
               >
                  Campaign Record
               </Button>
            </Grid>
         </Grid>
      </div>
   );
};

export default function LazyGo(): JSX.Element {
   const dis = useAppDispatch();
   const dialog: any = useAppSelector((state) => state.dialog);
   const campaign: any = useAppSelector((state) => state.campaign);
   const donation: any = useAppSelector((state) => state.donation);

   const openDia = (id: number, name: any, goal: any) => () => {
      const content = "";
      dis(
         setDialog({
            ...dialog,
            open: true,
            title: name,
            content,
            params: { id },
         })
      );
   };
   return (
      <>
         <div className='list'>
            <div style={{ marginBottom: 10 }}>
               <a href='/test'>Test</a>
            </div>

            {!campaign.arr ? (
               <>loading....</>
            ) : (
               campaign.arr.map((don: any) =>
                  don.status === "fraud" ? (
                     <div key={don.id}></div>
                  ) : (
                     <FadeInSection key={"id" + don.id}>
                        <DonRow
                           key={don.id}
                           id={don.id}
                           name={don.name}
                           description={don.description}
                           goal={don.goal}
                           status={don.status}
                           openDia={openDia}
                           donation={donation}
                        />
                     </FadeInSection>
                  )
               )
            )}
         </div>
      </>
   );
}
