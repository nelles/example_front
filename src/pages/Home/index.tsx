import React, { useEffect } from "react";

import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { setSnackbar } from "../../features/snackbar/snackbarSlice";
import { setDonation } from "../../features/donation/donationSlice";
import { apiPost } from "../../utilities/ApiRequest";
import { setCampaign } from "../../features/campaign/campaignSlice";
import { Grid } from "@mui/material";
import LazyLoadingGrid from "../../components/Lazy";
import LazyGo from "../../components/LazyGo";

export default function Home(): JSX.Element {
   const dispatch = useAppDispatch();
   const donation: any = useAppSelector((state) => state.donation);
   const campaign: any = useAppSelector((state) => state.campaign);
   const session: any = useAppSelector((state) => state.session);
   setTimeout(() => {
      // delay to allow parent render
      dispatch(setSnackbar({ isOpen: false }));

      (async () => {
         if (!donation.init) {
            dispatch(setDonation({ arr: [], init: true }));
            const donationRes = await apiPost("/donate_get");
            if (!donationRes.data.err && !donation.init) {
               dispatch(
                  setDonation({ arr: donationRes.data.data, init: true })
               );
            }
         }
      })();
      (async () => {
         if (!campaign.init) {
            const campaignRes = await apiPost("/campaigns_get");
            if (!campaignRes.data.err && !campaign.init) {
               dispatch(
                  setCampaign({ arr: campaignRes.data.data, init: true })
               );
            }
         }
      })();
   }, 100);

   useEffect(() => {
      //console.log("UE inside home");
   }, [campaign.arr]);

   return <>{campaign && campaign.init ? <LazyGo /> : <>loading...</>}</>;
}
