import React, { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { apiPost } from "../../utilities/ApiRequest";
import { setCampaign } from "../../features/campaign/campaignSlice";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import { API_URL } from "../../utilities/ApiRequest";

export default function Test(): JSX.Element {
   const campaign: any = useAppSelector((state) => state.campaign);
   const dispatch = useAppDispatch();

   const fraud = (name: number) => async () => {
      const path = window.open(`${API_URL}/MarkDonatorAsFraud/${name}`);
      setTimeout(() => (window.location.href = "/test"), 2000);
   };

   const unfraud = (name: number) => async () => {
      const path = window.open(`${API_URL}/unfraud/${name}`);
      setTimeout(() => (window.location.href = "/test"), 2000);
   };

   setTimeout(() => {
      (async () => {
         if (!campaign.init) {
            const campaignRes = await apiPost("/campaigns_get");
            if (!campaignRes.data.err && !campaign.init) {
               dispatch(
                  setCampaign({ arr: campaignRes.data.data, init: true })
               );
            }
         }
      })();
   }, 100);

   useEffect(() => {
      console.log("UE inside test");
      console.log(campaign.arr);
   }, [campaign.arr]);

   return (
      <>
         <a href='/home'>Home</a>
         <br />
         Campaigns:
         <Grid container spacing={2}>
            {campaign.arr.map((don: any) => (
               <Grid item key={don.id} md={12}>
                  {don.status === "fraud" ? (
                     <Button
                        variant='contained'
                        sx={{ m: 1, width: 200 }}
                        onClick={unfraud(don.name)}
                     >
                        Make unFraud
                     </Button>
                  ) : (
                     <Button
                        variant='contained'
                        sx={{ m: 1, width: 200 }}
                        color='error'
                        onClick={fraud(don.name)}
                     >
                        Make Fraud
                     </Button>
                  )}

                  {don.name}
               </Grid>
            ))}
         </Grid>
      </>
   );
}
