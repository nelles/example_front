import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

interface Arr {
   id: number;
   amount: string;
   nickname: string;
   state: string;
   cid: number;
}

export interface DonationState {
   arr: Arr[];
   init: boolean;
}

const initialState: DonationState = {
   arr: [],
   init: false,
};

export const donationSlice = createSlice({
   name: "donation",
   initialState,

   reducers: {
      setDonation: (state, action: PayloadAction<any>) => {
         try {
            state.init = action.payload.init;
            state.arr = action.payload.arr;
         } catch (error) {
            console.log(error);
         }
      },
      clearDonation: (state) => initialState,
   },
});

export const { setDonation, clearDonation } = donationSlice.actions;

export const selectDonation = (state: RootState) => state.donation;

export default donationSlice.reducer;
