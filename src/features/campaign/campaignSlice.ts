import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

interface Arr {
   id: number;
   name: string;
   description: string;
   goal: number;
   status: string;
}

export interface CampaignState {
   arr: Arr[];
   init: boolean;
}

const initialState: CampaignState = {
   arr: [],
   init: false,
};

export const campaignSlice = createSlice({
   name: "campaign",
   initialState,

   reducers: {
      setCampaign: (state, action: PayloadAction<any>) => {
         try {
            state.arr = action.payload.arr;
            state.init = action.payload.init;
         } catch (error) {
            console.log(error);
         }
      },
      clearCampaign: (state) => initialState,
   },
});

export const { setCampaign, clearCampaign } = campaignSlice.actions;

export const selectCampaign = (state: RootState) => state.campaign;

export default campaignSlice.reducer;
