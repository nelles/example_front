import { Suspense, useEffect } from "react";
import CssBaseline from "@mui/material/CssBaseline";
import { Routes, Route } from "react-router-dom";
import Test from "./pages/Test";
import Home from "./pages/Home";
import { SnackbarMsg } from "./components/SnackbarMsg";
import { useAppSelector } from "./app/hooks";
import { DialogState } from "./features/dialog/dialog";
import DiaWrap from "./components/DiaWrap";
import "./App.css";

function App() {
   const snackbar: any = useAppSelector((state) => state.snackbar);
   const dialog: DialogState = useAppSelector((state) => state.dialog);
   useEffect(() => {}, [snackbar]);
   return (
      <div style={{ padding: 20 }}>
         <Suspense fallback={<div>Loading...</div>}>
            <CssBaseline />
            <Routes>
               <Route path='/' element={<Home />} />
               <Route path='/home' element={<Home />} />
               <Route path='/test' element={<Test />} />
            </Routes>
         </Suspense>
         <SnackbarMsg snackbarState={snackbar} />
         {dialog && dialog.open ? <DiaWrap /> : <></>}
      </div>
   );
}

export default App;
